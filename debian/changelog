php-nyholm-psr7 (1.8.2-2) unstable; urgency=medium

  * Modernize PHPUnit syntax

 -- David Prévot <taffit@debian.org>  Mon, 17 Feb 2025 22:39:04 +0100

php-nyholm-psr7 (1.8.2-1) unstable; urgency=medium

  [ Ayesh Karunaratne ]
  * [PHP 8.4] Fixes for implicit nullability deprecation (#255)

  [ Tobias Nyholm ]
  * Changelog for 1.8.2 (#258)

  [ David Prévot ]
  * Update Standards-Version to 4.7.0

 -- David Prévot <taffit@debian.org>  Thu, 12 Sep 2024 08:51:24 +0200

php-nyholm-psr7 (1.8.1-3) unstable; urgency=medium

  * Compatibility with PHPUnit 11 (Closes: #1070581)
  * Revert "Force system dependencies loading" (updated pkg-php-tools)

 -- David Prévot <taffit@debian.org>  Sat, 03 Aug 2024 16:19:08 +0900

php-nyholm-psr7 (1.8.1-2) unstable; urgency=medium

  * Mark package as Multi-Arch: foreign
  * Force system dependencies loading

 -- David Prévot <taffit@debian.org>  Wed, 06 Mar 2024 17:56:22 +0100

php-nyholm-psr7 (1.8.1-1) unstable; urgency=medium

  * Upload to unstable now that php-psr-message 1.1 is available

  [ Nicolas Grekas ]
  * Fix error handling in Stream::getContents() (#252)

 -- David Prévot <taffit@debian.org>  Wed, 15 Nov 2023 07:42:53 +0100

php-nyholm-psr7 (1.8.0-1) experimental; urgency=medium

  [ Murat Erkenov ]
  * Deprecate HttplugFactory and make dependency on php-http/message-factory
    optional (#243)

  [ David Prévot ]
  * Use php-http-message-factory in CI

 -- David Prévot <taffit@debian.org>  Thu, 04 May 2023 19:26:28 +0200

php-nyholm-psr7 (1.7.0-1) experimental; urgency=medium

  [ Nicolas Grekas ]
  * Allow psr/http-message v2 (#234)

  [ David Prévot ]
  * Adapt version constraint to dh_phpcomposer expectations
  * Ignore Class Redeclarations in the same file

 -- David Prévot <taffit@debian.org>  Sat, 22 Apr 2023 11:45:59 +0200

php-nyholm-psr7 (1.6.1-1) experimental; urgency=medium

  [ Tobias Nyholm ]
  * Merge pull request from GHSA-wjfc-pgfp-pv9c [CVE-2023-29197], fixing
    improper input validation (Closes: #1034597)
  * Added changelog (#236)

 -- David Prévot <taffit@debian.org>  Tue, 18 Apr 2023 07:04:21 +0200

php-nyholm-psr7 (1.6.0-1) experimental; urgency=medium

  * Upload new minor to experimental during the freeze

  [ David Prévot ]
  * Drop now useless d/pkg-php-tools-* files
  * Update standards version to 4.6.2, no changes needed.

  [ Vano Devium ]
  * Uri: normalization leading slashes for getPath() (#211)

  [ Nicolas Grekas ]
  * Encode userinfo (but don't double-encode) (#213)
  * Improve extensibility by removing `@final` and
    making Stream's constructor public (#203)
  * Populate ServerRequest::getQueryParams() with
    query string passed to constructor (#214)
  * Stream: seek to the beginning of the string on initialization (#217)
  * Add some missing type checks on arguments (#220)
  * Release 1.6.0 (#218)

 -- David Prévot <taffit@debian.org>  Mon, 10 Apr 2023 09:33:22 +0200

php-nyholm-psr7 (1.5.1-1) unstable; urgency=medium

  [ Andrii Dembitskyi ]
  * Resolve symfony/error-handler deprecations (#197)

  [ Tobias Nyholm ]
  * Release 1.5.1 (#200)

  [ David Prévot ]
  * Update Standards-Version to 4.6.1

 -- David Prévot <taffit@debian.org>  Sat, 25 Jun 2022 11:39:24 +0200

php-nyholm-psr7 (1.5.0-1) unstable; urgency=medium

  * Upload to unstable now that Bullseye has been released

  [ Nicolas Grekas ]
  * Improve error handling (#185)
  * Add explicit `@return mixed` phpdoc (#187)

  [ Gabriel Ostrolucký ]
  * Add return types to HttplugFactory (#188)

  [ Tobias Nyholm ]
  * Release 1.5.0 (#194)

  [ David Prévot ]
  * Generate phpabtpl at build time
  * Update standards version to 4.6.0, no changes needed.

 -- David Prévot <taffit@debian.org>  Fri, 04 Feb 2022 05:35:35 -0400

php-nyholm-psr7 (1.4.1-1) experimental; urgency=medium

  [ Martijn van der Ven ]
  * Wrap fopen for PHP 8 (#174)

 -- David Prévot <taffit@debian.org>  Sun, 18 Jul 2021 17:39:10 +0200

php-nyholm-psr7 (1.4.0-1) experimental; urgency=medium

  * Upload to experimental to respect the freeze

  [ Tobias Nyholm ]
  * Remove LowercaseTrait (#172)
  * Remove final keyword (#169)

  [ David Prévot ]
  * Simplify gbp import-orig
  * Install upstream doc
  * Use dh-sequence-phpcomposer instead of pkg-php-tools

 -- David Prévot <taffit@debian.org>  Thu, 18 Feb 2021 20:43:24 -0400

php-nyholm-psr7 (1.3.2-2) unstable; urgency=medium

  * Fix d/clean
  * Only run Unit tests

 -- David Prévot <taffit@debian.org>  Sun, 13 Dec 2020 09:32:52 -0400

php-nyholm-psr7 (1.3.2-1) unstable; urgency=medium

  [ Tobias Nyholm ]
  * Improve exception message (#157)
  * Changes to meta files and support for PHPUnit 9 (#161)
  * Prepare release 1.3.2 (#158)

  [ David Prévot ]
  * Update watch file format version to 4.
  * Update Standards-Version to 4.5.1

 -- David Prévot <taffit@debian.org>  Sat, 21 Nov 2020 17:01:24 -0400

php-nyholm-psr7 (1.3.1-1) unstable; urgency=medium

  [ Tobias Nyholm ]
  * Allow the package to be installed on PHP8 (#151)

  [ David Prévot ]
  * Rename main branch to debian/latest (DEP-14)
  * Set Rules-Requires-Root: no.

 -- David Prévot <taffit@debian.org>  Tue, 22 Sep 2020 07:47:27 -0400

php-nyholm-psr7 (1.3.0-1) unstable; urgency=medium

  [ Tobias Nyholm ]
  * Prepare 1.3.0 (#147)

  [ David Prévot ]
  * Document patch forwarded upstream
  * Drop versioned dependency satisfied in (old)stable
  * Set upstream metadata fields:
    Bug-Database, Bug-Submit, Repository, Repository-Browse.
  * Update standards version to 4.5.0, no changes needed.
  * Use debhelper-compat 13
  * Simplify override_dh_auto_test
  * Adapt to new php-symfony-error-handler build-dependency

 -- David Prévot <taffit@debian.org>  Sat, 30 May 2020 11:47:52 -1000

php-nyholm-psr7 (1.2.1-2) unstable; urgency=medium

  * Fix CI dependencies

 -- David Prévot <taffit@debian.org>  Thu, 12 Sep 2019 14:38:49 -1000

php-nyholm-psr7 (1.2.1-1) unstable; urgency=medium

  [ Tobias Nyholm ]
  * Added changelog for 1.2.1 (#127)

  [ David Prévot ]
  * Update standards version, no changes needed.
  * Document gbp import-ref usage
  * Use upstream testsuite

 -- David Prévot <taffit@debian.org>  Sun, 08 Sep 2019 16:00:47 -1000

php-nyholm-psr7 (1.1.0-1) unstable; urgency=medium

  * Initial release (new symfony build-dependency)

 -- David Prévot <taffit@debian.org>  Sat, 11 May 2019 18:19:02 -1000
